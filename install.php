<!DOCTYPE html>
<?php
    header('Content-Type: text/html; charset= utf-8');
    $db = new SQLite3('animals.sqlite');
    if ($db->exec("CREATE TABLE 'animals'
               ('id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE ,
               'name' TEXT)")){
        $db->close();
    }
    Header("Location: index.php");
?>